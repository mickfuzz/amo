+++
title = "AMO - A New Online Archive Of UK Radical Media"
date = "2018-10-09"
slug =  "about"
+++


This online archive, produced from the Cowley Club in Brighton, is a new repository of UK Radical Media.

It features radical media produced in Britain going back to the late-60s, and every decade since (though, being an archive, its focus is old stuff, so isn’t concerned with recent years!)

These are its objectives from the outset:

**The types of material:** radical independent (written/printed) media going back over the past half century. The collection is centred on publications and websites coming mainly from community, activist or campaign media collectives. The range includes libertarian-left community papers from the late-60s through to the early 90s, direct action media from the 90s-2000s, and material from the post-2000 global anti-capitalist movements, to name some of the main parts of the archive.

Or, another way of looking at it: here’s a list of words, names and phrases (in no order) of the sorts of issues and politics covered by the publications featured, and there are many more which were omitted or extend from these:

...protection of environments and communities, autonomous social movements and community struggles (both local and international), feminism, anti-fascism, animal rights, anti-militarism, anti-capitalism, anti-racism, anarcho-left, squatting and housing, permaculture, indepedent radical media, Direct Action...

One significant feature of this archive is that through the Cowley Club’s collection, which goes back to the late-60-70s-80s (as well as other sources), this digitised archive offers snapshots going over a fifty-year period. So direct comparisons can be made between material produced in different eras - often to find that many of these same issues and concerns run through it all, even if emphases and attitudes vary during changing times.

At this initial stage, there is a predominance of Brighton publications, reflecting the rare local material in the Cowley Club archive, and the origins of this project, but this will change as more UK-wide material comes in, and hopefully as other archives around the country contribute, as they are welcome to. **So - calling all paper archives and people with collections around the country - get in touch.**

**Formats:** This partially splits the archive in half: before and after the Internet.

For publications which appeared only in print because that was all there was - i.e. anything before roughly the mid-90s - for the purposes of archiving this involves the digitisation of paper, and putting this into a readable form online, which includes providing indexing and ways to ease access and searchability of scanned text. Sometimes OCR (optical character recognition) is used, which turns scanned text into searchable computer text. For these older publications, the vast majority of what appears in this archive has been digitised here for the first time.

For publications produced since the early-90s, since the Internet, some of the focus is on re-hosting websites which had been offline for years for whatever reason. Added to this, where possible, is providing PDFs of their print output - which, if they’re from the computer era, can potentially be produced from the original DTP files, rather than paper scans. The priority with these publications and websites is to simply get them back into circulation so they appear in online searches.

## Contribute

Other collectives in social centres or community groups who have paper archives - and individuals with personal collections - are invited to contribute to this project, to both give it a wider spread of regions, as well as add chunks or rarities which otherwise might not have been available to us. (And share the load of all this scanning - it’s a slog!)

If you were involved in one of the publications or websites featured here, please get in touch - you may be able to contribute by giving us reference material or background information - even consider writing an article about it which could appear on this site.

### Copyright

All material remains copyright to its producer, and/or inherits the copyright license it was originally published with. All due effort has been made to contact producers, but in the instances where this hasn’t happened, please email if there’s a problem.
